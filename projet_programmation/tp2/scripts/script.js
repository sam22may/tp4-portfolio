'use strict'

var button = document.getElementById("run");

button.onclick = function () {

    const nom = "Samuel Le May"
    console.log(nom)
    document.getElementById("nom").innerHTML = nom

    const dateRapport = new Date()
    console.log(dateRapport.toLocaleDateString())
    document.getElementById("dateRapport").innerHTML = dateRapport.toLocaleDateString()
    // .toISOString()
    const dateNaissance = new Date(1990, 4, 22)
    document.getElementById("dateNaissance").innerHTML = dateNaissance.toLocaleDateString()
    let age = (dateRapport - dateNaissance) / (1000 * 60 * 60 * 24 * 365)
    console.log(Math.floor(age))
    document.getElementById("age").innerHTML = Math.floor(age)

    const sexe = "H"
    console.log(sexe)
    document.getElementById("sexe").innerHTML = sexe

    const situation_particuliere = "non"
    console.log(situation_particuliere)
    document.getElementById("situation_particuliere").innerHTML = situation_particuliere

    // condition max alcool
    let maxAlcoolJour
    let maxAlcoolSemaine
    let recommandationAlcoolRespectee

    if (situation_particuliere == "oui") {
        maxAlcoolSemaine = 0
        maxAlcoolJour = 0
    } else if (sexe == "H") {
        maxAlcoolJour = 3
        maxAlcoolSemaine = 15
    } else if (sexe == "F") {
        maxAlcoolJour = 2
        maxAlcoolSemaine = 10
    }
    document.getElementById("maxAlcoolJour").innerHTML = maxAlcoolJour
    document.getElementById("maxAlcoolSemaine").innerHTML = maxAlcoolSemaine

    // sur combien de jour et alcool par jour
    let nombreJour = parseInt(prompt("Sur combien de jours voulez vous faire votre analyse?"))
    let nbConsommationAlcoolJour
    let consommationTotal = 0
    let nbJoursExcedant = 0
    let nbJoursSans = 0
    const tableau = []
    for (let i = 0; i < nombreJour; i++) {
        nbConsommationAlcoolJour = parseInt(prompt("Écrire le nombre de consommoation pour chaque journée (jour" + (i + 1) + ")"))
        while (isNaN(nbConsommationAlcoolJour)) {
            nbConsommationAlcoolJour = parseInt(prompt("Écrire le nombre de consommoation pour chaque journée (jour" + (i + 1) + ")"))
        }
        console.log(nbConsommationAlcoolJour)
        tableau.push(nbConsommationAlcoolJour)
        consommationTotal = consommationTotal + nbConsommationAlcoolJour
        if (nbConsommationAlcoolJour > maxAlcoolJour) {
            nbJoursExcedant++
        }
        if (nbConsommationAlcoolJour == 0) {
            nbJoursSans++
        }
    }
    console.log(tableau)
    console.log("consommationTotal = "+consommationTotal)
    console.log("nbJoursExcedant = "+nbJoursExcedant)
    document.getElementById("consommationTotal").innerHTML = consommationTotal
    document.getElementById("nombreJour").innerHTML = nombreJour
    document.getElementById("tableau").innerHTML = tableau

    // le max pour chaque journée
    function trouverLeMax() {
        return Math.max(...tableau)
    }
    let maxConsommationJour = trouverLeMax()
    console.log("maxConsommationJour = "+maxConsommationJour)
    document.getElementById("maxConsommationJour").innerHTML = maxConsommationJour

    // le minimum pour chaque journée
    function trouverLeMinimum() {
        return Math.min(...tableau)
    }
    let minimumConsommationJour = trouverLeMinimum()
    console.log("minimumConsommationJour = "+minimumConsommationJour)
    document.getElementById("minimumConsommationJour").innerHTML = minimumConsommationJour

    // la moyenne de consommation
    function trouverLaMoyenne() {
        return consommationTotal / nombreJour
    }
    let moyenne = trouverLaMoyenne()
    moyenne = moyenne.toFixed(2)
    console.log("la moyenne = "+moyenne)
    document.getElementById("moyenne").innerHTML = moyenne

    // Ratio Excedant
    function trouverRatioExcedant() {
        return (nbJoursExcedant * 100) / nombreJour
    }
    let ratioExcedant = trouverRatioExcedant()
    ratioExcedant = ratioExcedant.toFixed(0)
    console.log("ratioExcedant = "+ratioExcedant)
    document.getElementById("ratioExcedant").innerHTML = ratioExcedant

    // Ratio sans alcool
    function trouverRatioSansAlcool() {
        return (nbJoursSans * 100) / nombreJour 
    }
    let ratioSans = trouverRatioSansAlcool()
    ratioSans = ratioSans.toFixed(0)
    console.log("ratioSans = "+ratioSans)
    document.getElementById("ratioSans").innerHTML = ratioSans


    // condition alcool respectee 
    const semaine = 7
    recommandationAlcoolRespectee = (maxAlcoolSemaine / semaine < consommationTotal / nombreJour) || (maxConsommationJour > maxAlcoolJour)

    if (recommandationAlcoolRespectee) {
        recommandationAlcoolRespectee = "Vous ne respectez pas les recommandations"
    } else {
        recommandationAlcoolRespectee = "Vous respectez les recommandations"
    }
    console.log(recommandationAlcoolRespectee)
    alert(recommandationAlcoolRespectee)
    document.getElementById("recommandationAlcoolRespectee").innerHTML = recommandationAlcoolRespectee

}
