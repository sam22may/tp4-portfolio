# TP2 - technique animation - Le portfolio animé - Samuel Le May

## Animations sur le site:

1. Tois animation par transition

- Sur éléments du menu(:hover):

    Toutes les pages dans le menu navigation en haut de page animation du lien.

    Page blog animation :hover sur la flèche/lien vers l'article.

    Page accueil :hover sur bouton abonne-toi

- Sur le scroll des pages:

    Page blog animation scroll down sur les cartes du blog.

    Page réalisations animation scroll down sur les boutons de navigation vers les projets.

- Sur un élément SVG:

    Page accueil animation sur le logo.

2. Une animation par Keyframes

    Page accueil animation sur le logo qui fait le tour de la page 3 fois.

3. Une animation contrôllée par javascript

    Page accueil animation sur le popup infolettre