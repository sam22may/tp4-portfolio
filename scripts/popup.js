const openPopupBtn = document.querySelectorAll('[data-popup-target]')
const closePopupBtn = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay')

openPopupBtn.forEach(button => {
    button.addEventListener('click', ()=>{
        const popup = document.querySelector(button.dataset.popupTarget)
        openPopup(popup)
    })
})

overlay.addEventListener('click', () =>{
    const popups = document.querySelectorAll('.popup.activePopup')
    popups.forEach(popup =>{
        closePopup(popup)
    })
})

closePopupBtn.forEach(button => {
    button.addEventListener('click', ()=>{
        const popup = button.closest('.popup')
        closePopup(popup)
    })
})

function openPopup(popup) {
    if(popup == null) return
    popup.classList.add('activePopup')
    overlay.classList.add('activeOverlay')
}

function closePopup(popup){
    if(popup == null) return
    popup.classList.remove('activePopup')
    overlay.classList.remove('activeOverlay')
}