anime({
    targets: '#typo path',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'easeInOutSine',
    duration: 15500,
    delay: function(el, i) { return i * 250 },
    direction: 'alternate',
    loop: false
  });