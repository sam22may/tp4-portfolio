
let error = []
const form = document.getElementById('formulaire')
const prenom = document.getElementById('prenom')
const nom = document.getElementById('nom')
const email = document.getElementById('courriel')
const emailValide = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const telephone = document.getElementById('telephone')
const telValide = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/
const sujet = document.getElementById('sujet')
const demande = document.getElementById('demande')
const errorContainer = document.querySelector('.error-list')
const chaine_a_valider = [prenom, nom, sujet]
const errorField = document.getElementById('error-field')
const captcha = document.getElementById('captcha')

form.addEventListener('submit', function(e){
    errorField.textContent = " "
    error = []
    validateStringInput()
    validateEmailInput()
    validateTelephone()
    validateTextArea()
    validateCaptcha()

    if(error.length > 0){
        errorContainerManager()
        e.preventDefault()
    }
})

function validateStringInput(){
    for(let i = 0; i < chaine_a_valider.length; i++){
        let chaine = chaine_a_valider[i]
        if (chaine.value.length <= 0){
            error.push(`Vous devez écrire un ${chaine.id}`)
            errorCaller(chaine)
        } else {
            errorRemove(chaine)
        }
    }
}

function errorCaller(champ){
    champ.classList.add('input-error')
}

function errorRemove(champ){
    champ.classList.remove('input-error')
}

function validateEmailInput(){
    if(email.value.length <= 0){
        error.push(`Veillez écrire un ${email.id}`)
        errorCaller(email)
    }
    if(!emailValide.test(email.value)){
        error.push(`Le ${email.id} doit être d'un format valide`)
        errorCaller(email)
    } else {
        errorRemove(email)
    }
}

function validateTelephone(){
    if(telephone.value.length <= 0){
        error.push(`Veillez écrire un ${telephone.id}`)
        errorCaller(telephone)
    }
    if(!telValide.test(telephone.value)){
        error.push(`Le ${telephone.id} doit être d'un format valide (000-000-0000)`)
        errorCaller(telephone)
    } else {
        errorRemove(telephone)
    }
}

function validateTextArea(){
    let spaceless = demande.value.replace(/\s/g, '')
    if (spaceless.length <= 0){
        error.push(`Veillez spécifier les détails de votre demande`)
        errorCaller(demande)
    } else {
      errorRemove(demande)
    }
}

function validateCaptcha(){
    if(captcha.value != 5){
        error.push(`Veillez répondre à l'équation (5+2=?)`)
        errorCaller(captcha)
    } else {
        errorRemove(captcha)
    }
}

function errorContainerManager(){
    for (err in error){
        let tag = document.createElement('p')
        let error_text = document.createTextNode(error[err])
        tag.appendChild(error_text)
        errorContainer.appendChild(tag)
        errorContainer.classList.add('error-list-visible')
    }
}